<?php // routes/breadcrumbs.php

// Note: Laravel will automatically resolve `Breadcrumbs::` without
// this import. This is nice for IDE syntax and refactoring.
use Diglactic\Breadcrumbs\Breadcrumbs;

// This import is also not required, and you could replace `BreadcrumbTrail $trail`
//  with `$trail`. This is nice for IDE type checking and completion.
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('Home', route('home'));
});

Breadcrumbs::for('contacto', function (BreadcrumbTrail $trail) {
    $trail->push('Contacto', route('contacto'));
});

Breadcrumbs::for('/folios', function (BreadcrumbTrail $trail) {
    $trail->push('Gestión de folios', route('folios'));
});

Breadcrumbs::for('consulta', function (BreadcrumbTrail $trail) {
    $trail->push('Consulta IN VISU', route('consulta'));
});

Breadcrumbs::for('historial', function (BreadcrumbTrail $trail) {
    $trail->push('Auditoría de consultas realizadas', route('historial'));
});

Breadcrumbs::for('usuarios', function (BreadcrumbTrail $trail) {
    $trail->push('Gestión de usuarios', route('usuarios'));
});

Breadcrumbs::for('roles', function (BreadcrumbTrail $trail) {
    $trail->push('Gestión de roles', route('roles'));
});

Breadcrumbs::for('registroCivil', function (BreadcrumbTrail $trail) {
    $trail->push('Gestión de registros', route('registroCivil'));
});

Breadcrumbs::for('folios/create', function (BreadcrumbTrail $trail) {
    $trail->parent('folios');
    $trail->push('Carga de folios');
});

Breadcrumbs::for('folios/{id}/edit', function (BreadcrumbTrail $trail) {
    $trail->parent('folios');
    $trail->push('Editar folio', route('folios'));
});