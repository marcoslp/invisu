<?php

use App\Http\Controllers\FolioController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\RegistroCivilController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function(){return view('auth.login');});
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('folios/limpiar', [FolioController::class, 'limpiar'])->name('folios.limpiar');
    Route::get('historial/limpiarHistorial', [FolioController::class, 'limpiar_hisrial'])->name('folios.limpiarHistorial');
    Route::resource('roles', RolController::class);
    Route::resource('usuarios', UsuarioController::class);
    Route::resource('folios', FolioController::class);
    Route::resource('registroCivil', RegistroCivilController::class);
    Route::get('/consulta', [FolioController::class, 'getFormularioConsulta'])->name('consulta');
    Route::post('/consulta', [FolioController::class, 'postFormularioConsulta'])->name('postconsulta');
    Route::get('/error/{registro_id}', [FolioController::class, 'getErrorConsulta'])->name('errorconsulta');
    // Route::post('/historial', [FolioController::class, 'postHistorial'])->name('historialComprobantes');
    Route::get('/historial', [FolioController::class, 'postHistorial'])->name('historialComprobantes');
    Route::get('/descargar', [FolioController::class, 'postdescargar'])->name('postdescargar');
    // Route::post('/descargar', [FolioController::class, 'postdescargar'])->name('postdescargar');
    Route::get('/contacto', [HomeController::class, 'contacto'])->name('contacto');
    Route::get('folios/{folio}/delete', [FolioController::class, 'destroy'])->name('folios.delete');
    Route::get('registroCivil/{registroCivil}/delete', [RegistroCivilController::class, 'destroy'])->name('registroCivil.delete');
    Route::get('roles/{rol}/delete', [RolController::class, 'destroy'])->name('roles.delete');
    Route::get('usuarios/{usuario}/delete', [UsuarioController::class, 'destroy'])->name('usuarios.delete');
    Route::get('/registroCivilImportar/{registroCivil}', [RegistroCivilController::class, 'importarEscaneados'])->name('registroCivil.importar');
});
