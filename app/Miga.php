<?php

	function mostrar_miga($navegacion) {
		$miga = [];
		foreach ($navegacion as $link) {
				if (!empty($link['link'])) {
					$miga[] = '<a class="navbar-brand" href="'.$link['link'].'" style="color: #799f4f;font-size:0.875rem;">'.$link['titulo'].'</a>';
				} else {
					$miga[] = '<span class="navbar-brand" style="font-size:0.875rem;">'.$link['titulo'].'</span>';
				}
		}
		
		echo '<nav class="navbar navbar-expand-lg navbar-light" style="border-bottom:1px solid #e3e3e3;line-height: 1.3; background-color:#FFFFFF;margin-left:-15px;margin-right:-15px;padding-left:23px;">';
		echo implode('<span class="navbar-brand" style="font-size:0.875rem;">/</span>', $miga);
		echo '</nav>';
	}