<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\folio;

class FolioUnico implements Rule
{
    private $registro_id;

    public function __construct($reg_id)
    {
        $this->registro_id = $reg_id;
    }

    public function passes($attribute, $value)
    {
        $existe=Folio::Where('numeroMatricula', $value)
                    ->where('registro_id',$this->registro_id)
                    ->first();
        return empty($existe);
    }

    public function message()
    {
        return 'El número de matrícula ya está siendo utilizado en este registro.';
    }
}

