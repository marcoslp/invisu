<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DifferentFile implements Rule
{
    private $hash;

    public function __construct($hash)
    {
        $this->hash = $hash;
    }

    public function passes($attribute, $value)
    {
        return hash_file('sha256', $value) != $this->hash;
    }

    public function message()
    {
        return 'El archivo :attribute debe ser diferente al archivo Orden de pago.';
    }
}

