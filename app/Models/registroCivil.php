<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class registroCivil extends Model
{
    use HasFactory;
    protected $fillable = ['codigoRegistro', 'nombreRegistro', 'emailRegistro', 'carpetaRegistro', 'donde_consultar'];
}
