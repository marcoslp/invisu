<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comprobantes extends Model
{
    use HasFactory;
    protected $fillable = ['ordenPago', 'comprobantePago', 'usuarioConsultor', 'registro_id', 'matricula_id', 'resultadoBusqueda'];
}
