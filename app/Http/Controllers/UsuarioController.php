<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\Models\registroCivil;
use App\Models\comprobantes;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        $input = [];
        if (session()->has('operacion') && session()->get('operacion') == 'usuarios' && !empty(session()->get('filtros'))) {
            $input = (!empty($request->all()) && session()->get('filtros') != $request->all())?$request->all():session()->get('filtros');
        } else {
            session(['operacion' => 'usuarios', 'filtros' => empty($request->all())?$input:$request->all()]);
            $input = session()->get('filtros');
        }

        if (!empty($input) || $request->hasAny(['busquedaRol', 'busquedaDNI', 'busquedaNombre', 'busquedaEmail', 'busquedaEstado'])) {
            $query = User::with("roles");
            if (!empty($input['busquedaRol'])) $query->whereRelation('roles', 'role_id', '=', $input['busquedaRol']);
            if (!empty($input['busquedaDNI'])) $query->where('documento', '=', $input['busquedaDNI']);
            if (!empty($input['busquedaNombre'])) $query->where('name', 'like', '%'.$input['busquedaNombre'].'%');
            if (!empty($input['busquedaEmail'])) $query->where('email', 'like', '%'.$input['busquedaEmail'].'%');
            if (!empty($input['busquedaEstado'])) $query->where('estado', '=', $input['busquedaEstado']);
            if (!empty($input['busquedaRegistro'])) $query->where('registro_id', '=', $input['busquedaRegistro']);
            $query->orderBy('name');
            $usuarios = $query->paginate(50);
        } else {
            $usuarios = array();
        }
        $roles = DB::table('roles')->orderBy('name','asc')->pluck('name','id');
        $registros = DB::table('registro_civils')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');
        return view('usuarios.index', compact('usuarios', 'roles', 'registros', 'input'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Aca es donde voy a levantar el formulario de carga
        $roles = DB::table('roles')->orderBy('name','asc')->pluck('name','id');
        $registros = DB::table('registro_civils')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');
        return view('usuarios.crear', compact('roles', 'registros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Aca se procesa todo lo que sea logica para luego guardar registros
        $this->validate($request, [
            'name' => 'required|min:5|max:50|regex:/^(?=.{3,50}$)[a-zñA-ZÑ](\s?[a-zñA-ZÑ])*$/',
            'documento' => 'required|min:5|max:12|unique:users,documento',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password|regex:^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$^',
            'roles' => 'required',
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        toastr()->success('Usuario creado exitosamente !!', 'Aviso!');
        
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = DB::table('roles')->orderBy('name','asc')->pluck('name','id');
        $registros = DB::table('registro_civils')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');
        $userRole = null;
        if (count($user->roles) > 0) $userRole = $user->roles[0]->id;
        
        return view('usuarios.editar', compact('user', 'roles', 'userRole', 'registros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //similar al store, es para guardar en BD lo que vamos a editar
        $validator = $this->validate($request, [
            'name' => 'required',
            'documento' => 'required|min:5|max:12|unique:users,documento,' . $id,
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'sometimes',
        ]);
        if (!empty($request->password)) {
            $validator = $this->validate($request, [
                'password' => 'same:confirm-password|regex:^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$^',
            ]);
        }
        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }
        
        $user = User::find($id);
        
        $user->update($input);
        if (!empty($request->roles)) {
            DB::table('model_has_roles')->where('model_id', $id)->delete();
            $user->assignRole($request->input('roles'));
        }
        toastr()->success('Usuario modificado exitosamente !!', 'Aviso!');

        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario=User::find($id);
        if (!empty($usuario)) {
            $usuario->estado = $usuario->estado=='activo'?'inactivo':'activo';
            $usuario->update();
            toastr()->success('Estado modificado exitosamente !!', 'Aviso!');
        }else{
            toastr()->error('Usuario no encontrado!!', 'Error!');
        }
        
        return redirect()->route('usuarios.index');
    }  
}
