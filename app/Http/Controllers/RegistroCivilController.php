<?php

namespace App\Http\Controllers;

use App\Models\folio;
use App\Models\registroCivil;
use App\Models\Comprobantes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class RegistroCivilController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:ver-registros|crear-registros|editar-registros|borrar-registros', ['only' => ['index']]);
        $this->middleware('permission:crear-registros', ['only' => ['create', 'store']]);
        $this->middleware('permission:editar-registros', ['only' => ['edit', 'update']]);
        $this->middleware('permission:borrar-registros', ['only' => ['destroy']]);
    }

    public function index()
    {
        $registros = registroCivil::orderBy('nombreRegistro')->get();
        
        $registros_enviar = [];
        foreach($registros as $registro) {
            $registro_enviar = $registro;
            $folio = Folio::where('registro_id', $registro->id)->first();
            if (!empty($folio)) $registro_enviar->carpetaRegistro = null;
            $registros_enviar[] = $registro_enviar;
        }
        $registros = $registros_enviar;
        $donde_consultar = ['ws' => 'Sist. de Informatica', 'base' => 'Sist. consulta IN VISU'];
        return view('registros.index', compact('registros', 'donde_consultar'));
    }

    public function create()
    {
        $carpetas = Storage::directories('Archivos');
        $carpetasRegistros = [];
        foreach ($carpetas as $ruta) {
            $ruta_carpeta = explode('/', $ruta);
            $nombreCarpeta = array_pop($ruta_carpeta);
            $carpetasRegistros[$nombreCarpeta] = $nombreCarpeta;
        }
        $donde_consultar = ['ws' => 'Sist. de Informatica', 'base' => 'Sist. consulta INVISU'];
        return view('registros.crear', compact('carpetasRegistros', 'donde_consultar'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'codigoRegistro' => 'required',
            'nombreRegistro' => 'required|max:30|min:3',
            'emailRegistro' => 'required|email|max:50|min:3']
        );
        $registros = registroCivil::create($request->all());
        toastr()->success('Registro creado exitosamente !!', 'Aviso!');
        return redirect()->route('registroCivil.index');
    }

    public function show(registroCivil $registroCivil)
    {
        //
    }

    public function edit(registroCivil $registroCivil)
    {
        $registros = registroCivil::find($registroCivil->id);
        
        $carpetas = Storage::directories('Archivos');
        $carpetasRegistros = [];
        foreach ($carpetas as $ruta) {
            $ruta_carpeta = explode('/', $ruta);
            $nombreCarpeta = array_pop($ruta_carpeta);
            $carpetasRegistros[$nombreCarpeta] = $nombreCarpeta;
        }
        $donde_consultar = ['ws' => 'Sist. de Informatica', 'base' => 'Sist. consulta INVISU'];
        return view('registros.editar', compact('registros', 'carpetasRegistros', 'donde_consultar'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombreRegistro' => 'required|max:30|min:3',
            'emailRegistro' => 'required|email|max:50|min:3'
        ]);
        $registros = registroCivil::find($id);
        $registros->update($request->all());
        toastr()->success('Registro modificado exitosamente !!', 'Aviso!');
        return redirect()->route('registroCivil.index');
    }

    public function destroy(registroCivil $registroCivil)
    {
        $existe=0;//le pongo 0 para despues comprobarlo con el empty
        
        $folios = Folio::Where('folios.registro_id','=',$registroCivil->id)->first();
        $comprobantes = Comprobantes::where('comprobantes.registro_id', '=', $registroCivil->id)->first();
        if (empty($folios) && empty($comprobantes)) {
            DB::table('registro_civils')->where('id', $registroCivil->id)->delete();
            toastr()->success('Registro borrado exitosamente!', 'Aviso!');
        } else {
            throw ValidationException::withMessages(["registro" => "El registro que intenta borrar está siendo utilizado"]);
        }
        return redirect()->route('registroCivil.index');
    }
    
    public function importarEscaneados(Request $request, $id)
    {
        $registro = registroCivil::find($id);
        $archivos = Storage::allFiles ('Archivos/'.$registro->carpetaRegistro);
        $i = 0;
        if (!empty($archivos) && !empty($registro)) {
            foreach ($archivos as $archivo) {
                $ruta_archivo = explode('/', $archivo);
                array_shift($ruta_archivo);
                $ruta_guardar = implode('/', $ruta_archivo);
                list($nombre, $extension) = explode('.', array_pop($ruta_archivo));
                $nombre = str_replace('FOLIO ', '', $nombre);
                $folio = Folio::where('registro_id', $registro->id)->where('numeroMatricula', $nombre)->first();
                if (empty($folio)) {
                    $folio = Folio::create();
                    $folio->numeroMatricula = $nombre;
                    $folio->registro_id = $registro->id;
                    $folio->fechaScaneo = date('Y-m-d');
                    $folio->estado='activo';
                    $folio->archivo = $ruta_guardar;
                    $folio->save();
                    $i++;
                }
            }
        }
        if ($i > 0)
            toastr()->success("Se importaron $i Folios!", 'Aviso');
        else 
            toastr()->success("No hay folios importar!", 'Aviso');
        return redirect()->route('registroCivil.index');
    }    
}
