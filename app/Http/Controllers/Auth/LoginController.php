<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    protected function authenticated(Request $request, $user)
    {        
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
        if (empty($request->get('g-recaptcha-response'))) {
            Auth::logout();
            return redirect()->back()->withInput()->withErrors(['captcha' => 'Por favor tilde la opción "No soy un robot".']);
        }
        
        if ($user->estado=='activo') {
            return redirect()->intended($this->redirectPath());
        } else {
            Auth::logout();
            return redirect()->back()->withInput()->withErrors(['active' => 'Tu cuenta ha sido desactivada.']);
        }        
    }
    
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/login');
    }

}
