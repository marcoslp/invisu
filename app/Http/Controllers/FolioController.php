<?php

namespace App\Http\Controllers;

use App\Models\Comprobantes;
use App\Models\folio;
use App\Models\registroCivil;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use DateTime;
use DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Response;
use App\Rules\DifferentFile;
use App\Rules\FolioUnico;

class FolioController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:ver-folio|crear-folio|editar-folio|borrar-folio', ['only' => ['index']]);
        $this->middleware('permission:crear-folio', ['only' => ['create', 'store']]);
        $this->middleware('permission:editar-folio', ['only' => ['edit', 'update']]);
        $this->middleware('permission:borrar-folio', ['only' => ['destroy']]);
    }
    
    public function limpiar(Request $request)
    {
      session()->forget(['operacion', 'filtros']);
      return redirect()->route('folios.index');
    }
    
    public function limpiar_hisrial(Request $request)
    {
      session()->forget(['operacion', 'historial']);
      return redirect()->route('historialComprobantes');
    }
    
    public function index(Request $request)
    {
        $input = [];
        if (session()->has('operacion') && session()->get('operacion') == 'folios' && !empty(session()->get('filtros'))) {
            $input = (!empty($request->all()) && session()->get('filtros') != $request->all())?$request->all():session()->get('filtros');
        } else {
            session(['operacion' => 'folios', 'filtros' => empty($request->all())?$input:$request->all()]);
            $input = session()->get('filtros');
        }

        if (!empty($input) || $request->hasAny(['busquedaMatricula', 'busquedaFechaScaneoI', 'busquedaFechaScaneoF', 'busquedaRegistro_id'])) {
            
            $query = 
                Folio::join('registro_civils', 'registro_civils.id', 'folios.registro_id')
                    ->select('folios.*', 'registro_civils.nombreRegistro', 'registro_civils.codigoRegistro', 'registro_civils.donde_consultar as origen');

            if (!empty($input['busquedaMatricula'])) {
                $query->Where('folios.numeroMatricula', 'like', '%' . $input['busquedaMatricula'] . '%');
            }
            
            if (!empty($input['busquedaFechaScaneoI']) && !empty($input['busquedaFechaScaneoF'])) {
              if ($input['busquedaFechaScaneoF'] < $input['busquedaFechaScaneoI']) {
                throw ValidationException::withMessages(["busquedaFechaScaneoF" => "La fecha de fin debe ser mayor que la fecha de inicio"]);
              }
            }
            
            if (!empty($input['busquedaFechaScaneoI'])) {
              $query->whereDate('folios.updated_at', '>=', new DateTime($input['busquedaFechaScaneoI']));
            }
            
            if (!empty($input['busquedaFechaScaneoF'])) {
              $query->whereDate('folios.updated_at', '<=', new DateTime($input['busquedaFechaScaneoF'].' 23:59:59'));
            }
            
            if (!empty($input['busquedaRegistro_id'])) {
                $query->Where('folios.registro_id', '=', $input['busquedaRegistro_id']);
            }
            
            if (!empty(Auth::user()->registro_id)) {
                $query->Where('folios.registro_id', '=', Auth::user()->registro_id);
            }
            
            $query->where('folios.estado','<>','inactivo');
            $query->where('registro_civils.donde_consultar','base');
            $query->orderBy('folios.updated_at', 'desc');
            $folios=$query->paginate(50);
            $folios->setPath('');
            // dd($folios);
        } else {
            $folios = array();
            $input = [
                'busquedaMatricula' => null, 
                'busquedaFechaScaneoI' => null, 
                'busquedaFechaScaneoF' => null, 
                'busquedaRegistro_id' => null
            ];            
        }

        $registros = DB::table('registro_civils')->where('donde_consultar', 'base')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');
        return view('folios.index', compact('folios', 'registros', 'input'));
    }

    public function create()
    {
        //Metodo donde levantamos formulario para crear
        $registros = DB::table('registro_civils')->where('donde_consultar', 'base')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');
        return view('folios.crear', compact('registros'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'registro_id' => 'required|exists:registro_civils,id',
            'numeroMatricula' => 'required|max:25|min:1',
            'archivo' => 'required|max:5000|mimes:pdf'
        ]);
        request()->validate(['numeroMatricula' => [new FolioUnico($request->registro_id)]]);

        $registro = registroCivil::find($request->registro_id);
        $folio = Folio::create();
        $folio->numeroMatricula = $request->numeroMatricula;
        $folio->registro_id = $request->registro_id;
        $folio->fechaScaneo = date('Y-m-d');
        $folio->estado='activo';
        $nombreArchivo = md5($request->registro_id.$request->numeroMatricula.date('Ymdyis')).'.pdf';
        $folio->archivo = $registro->carpetaRegistro.'/'.$nombreArchivo;
        $folio->save();
        $path = $request->file('archivo')->storeAs('Archivos/'.$registro->carpetaRegistro, $nombreArchivo);
        toastr()->success('Folio creado exitosamente!', 'Aviso');        
        return redirect()->route('folios.index');
    }

    public function edit(folio $folio)
    {
        //Lo voy a usar para levantar el formulario de busqueda
        $registros = DB::table('registro_civils')->where('donde_consultar', 'base')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');
        $folio->fechaScaneo = date('Y-m-d', strtotime($folio['fechaScaneo']));
        return view('folios.editar', compact('registros', 'folio'));
    }

    public function update(Request $request, folio $folio)
    {
        //Este funciona como el store pero para guardar datos
        request()->validate([
            'numeroMatricula' => 'required|max:25',
            'registro_id' => 'required',
        ]);
        request()->validate(['numeroMatricula' => [new FolioUnico($request->registro_id)]]);
        
        $folio->update($request->all());
        if (!empty($request->archivo)) {
            $registro = registroCivil::find($request->registro_id);
            $nombreArchivo = md5($request->registro_id.$request->numeroMatricula.date('Ymdhis')).'.pdf';
            $folio->archivo = $registro->carpetaRegistro.'/'.$nombreArchivo;
            $path = $request->file('archivo')->storeAs('Archivos/'.$registro->carpetaRegistro, $nombreArchivo);          
            $folio->updated_at = date('Y-m-d h:i:s');
            $folio->save();
        }
        toastr()->success("Folio modificado exitosamente!", 'Aviso');
        return redirect()->route('folios.index');
    }

    public function destroy(folio $folio)
    {
        $folio = Folio::find($folio->id);
        $folio->estado ='inactivo';
        $folio->update();
        toastr()->success("Folio borrado exitosamente!", 'Aviso');
        return redirect()->route('folios.index');
    }
    
    //Funcion que arma el formulario o vista para realizar la consulta
    public function getFormularioConsulta()
    {
        $registros = DB::table('registro_civils')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');
        $folios = [];
        return view('folios.consultar', compact('registros', 'folios'));

    }
    
    //Funcion que busca y devuelve el folio consultado, ademas almacena los comprobantes
    public function postFormularioConsulta(Request $request)
    {
        //Metodo donde se hace todo el trabajo de guardado post consulta
        request()->validate([
            'matricula_id' => 'required|max:25|min:1',
            'registro_id' => 'required',
            'ordenPago' => 'required|file|max:2048|mimes:jpg,jpeg,png,pdf',
            'comprobantePago' => 'required|file|max:2048|mimes:jpg,jpeg,png,pdf',
        ], [
            'ordenPago.max' => 'El tamaño máximo del archivo de Orden de pago debe ser de 2 Megabytes.',
            'comprobantePago.max' => 'El tamaño máximo del archivo de Comprobante de pago debe ser de 2 Megabytes.',
        ]);
        request()->validate([
            'comprobantePago' => [new DifferentFile(hash_file('sha256', $request->file('ordenPago')))]
        ]);
        
        if ($request) {
            $nombreOrdenPago = md5('ordenPago-' . $request['matricula_id'] . '-' . date("Ymdhis")) . '.' . $request->file('ordenPago')->clientExtension();
            $request->file('ordenPago')->storeAs('Comprobantes', ($nombreOrdenPago));

            $nombreComprobantePago = md5('comprobantePago-' . $request['matricula_id'] . '-' . date("Ymdhis")) . '.' . $request->file('comprobantePago')->clientExtension();
            $request->file('comprobantePago')->storeAs('Comprobantes', ($nombreComprobantePago));

            $comprobante = new Comprobantes;
            $comprobante->ordenPago = $nombreOrdenPago;
            $comprobante->comprobantePago = $nombreComprobantePago;
            $comprobante->matricula_id = $request['matricula_id'];
            $comprobante->registro_id = $request['registro_id'];
            $comprobante->usuarioConsultor = Auth::user()->id;
            
            $registro = registroCivil::find($request->registro_id);
            // dd($registro);
            if ($registro->donde_consultar == 'base') {
                $resultado = Folio::select('numeroMatricula as numero', 'nombreRegistro as registro', DB::raw("DATE_FORMAT(folios.updated_at, '%d/%m/%Y') as fecha_alta"), 'folios.id', 'donde_consultar as origen')
                    ->join('registro_civils', 'folios.registro_id', '=', 'registro_civils.id')
                    ->where('numeroMatricula', 'like', '%'.$request['matricula_id'].'%')
                    ->where('registro_id', '=', $request['registro_id'])
                    ->where('estado', '=', 'activo')
                    ->orderBy('numeroMatricula', 'asc')
                    ->orderBy('folios.updated_at', 'desc')
                    ->get();
            } else {
                try {
                    $cliente = new Client(['headers' => ['Authorization' => 'Basic d3Npbm11ZWJsZXM6dyRycDEyMDIy']]);
                    $response = $cliente->post(
                         "https://apps2.entrerios.gov.ar/rpi-api/token"
                    )->getBody();
                    $resultado = json_decode($response);            
                
                    $cliente = new Client(['headers' => ['Authorization' => 'Bearer '.$resultado->token]]);
                    $response = $cliente->get(
                         'https://apps2.entrerios.gov.ar/rpi-api/inmuebles/buscar/folio?matricula='.ltrim($request['matricula_id'], '0').'&entidad='.$registro->codigoRegistro
                    )->getBody();
                    $resultado = json_decode($response);
                } catch(\Exception $e) {
                    // $error = $e->getResponse();
                    // dd($error->getStatusCode());
                    $resultado = null;
                }
                // dd($resultado);
                if (!empty($resultado[0])) {
                    foreach ($resultado as $i => $r) {
                        $Base64Img = base64_decode($resultado[$i]->imagen);
                        $archivo = md5($resultado[$i]->registro.$resultado[$i]->numero.$resultado[$i]->fecha_alta.date('Ymdhis')).'.pdf';
                        Storage::disk('archivador')->put($archivo, $Base64Img); 
                        $resultado[$i]->id = $archivo;
                        $resultado[$i]->numero = empty($resultado[$i]->ufuncional)?$resultado[$i]->numero:$resultado[$i]->numero.' UF'.$resultado[$i]->ufuncional;
                        $resultado[$i]->origen = 'ws';
                    }
                } else $resultado = null;
            }
            
            session()->forget('descargado');
            
            if (!empty($resultado[0])) {
                $comprobante->resultadoBusqueda = "Encontrado";
                $comprobante->save();
                return view('folios.resultado', compact('resultado'));
            } else {
                $comprobante->resultadoBusqueda = "No se encontró";
                $comprobante->save();
                $mensaje='La matrícula ingresada no se encuentra en el '. $registro->nombreRegistro .' o bien no se encuentra digitalizada. Para más información contáctenos a la siguiente dirección de mail: '.$registro->emailRegistro;
                $mensajeInfo='Recuerde en el correo poner sus datos personales. Nombre, Apellido, DNI, Teléfono, EMAIL. Además debe adjuntar el Comprobante y la Orden.';
                return view('folios.resultado', compact('resultado', 'mensaje', 'mensajeInfo'));
            }
        }

    }

    public function postHistorial(Request $request)
    {
        $input = [];
        if (session()->has('operacion') && session()->get('operacion') == 'historial' && !empty(session()->get('filtros'))) {
            $input = (!empty($request->all()) && session()->get('filtros') != $request->all())?$request->all():session()->get('filtros');
        } else {
            session(['operacion' => 'historial', 'filtros' => empty($request->all())?$input:$request->all()]);
            $input = session()->get('filtros');
        }

        if (!empty($input) || $request->hasAny(['busquedaMatricula', 'busquedaUsuario', 'busquedaResultado', 'busquedaFechaI', 'busquedaFechaF', 'busquedaRegistro_id'])) {
            $query = Comprobantes::select('ordenPago', 'comprobantePago', 'usuarioConsultor', 'resultadoBusqueda', 'comprobantes.created_at', 'users.name', 'registro_civils.nombreRegistro', 'matricula_id')
            ->join('users', 'users.id', 'comprobantes.usuarioConsultor')
            ->join('registro_civils', 'registro_civils.id', 'comprobantes.registro_id')
            ->where('usuarioConsultor', '>=', '0');

            if (!empty($input['busquedaMatricula'])) {
                $query->Where('comprobantes.matricula_id', 'like', '%' . $input['busquedaMatricula'] . '%');
            }

            if (!empty($input['busquedaUsuario'])) {
                $query->Where('users.name', 'like', '%' . $input['busquedaUsuario'] . '%');
            }
            
            if (!empty($input['busquedaResultado'])) {
                $query->Where('comprobantes.resultadoBusqueda', 'like', $input['busquedaResultado']); 
            }

            if (!empty($input['busquedaFechaI']) && !empty($input['busquedaFechaF'])) {
              if ($input['busquedaFechaF'] < $input['busquedaFechaI']) {
                throw ValidationException::withMessages(["busquedaFechaF" => "La fecha de fin debe ser mayor que la fecha de inicio"]);
              }
            }
            
            if (!empty($input['busquedaFechaI'])) {
              $query->whereDate('comprobantes.updated_at', '>=', new DateTime($input['busquedaFechaI']));
            }
            
            if (!empty($input['busquedaFechaF'])) {
              $query->whereDate('comprobantes.updated_at', '<=', new DateTime($input['busquedaFechaF'] . ' 23:59:59'));
            }
            
            if (!empty($input['busquedaRegistro_id'])) {
                $query->Where('comprobantes.registro_id', '=', $input['busquedaRegistro_id']);
            }

            if (Auth::user()->id != 1) {
                $query->Where('comprobantes.usuarioConsultor', '=', Auth::user()->id);
            }
            
            $query->orderBy('comprobantes.created_at', 'desc');
            $comprobantes = $query->paginate(30);
            $comprobantes->setPath('');
        } else {
            $comprobantes = array();
            $input = [
                'busquedaMatricula' => null, 
                'busquedaUsuario' => null, 
                'busquedaResultado' => null, 
                'busquedaFechaI' => null,
                'busquedaFechaF' => null,
                'busquedaRegistro_id' => null
            ];
        }
        $registros = DB::table('registro_civils')->orderBy('nombreRegistro','asc')->pluck('nombreRegistro','id');

        return view('folios.historial', compact('comprobantes','registros', 'input'));
    }
    
    public function postdescargar(Request $request)
    {
        if (!empty($request['id']) && !empty($request['origen'])) {
            if (!session()->has('descargado') || Auth::User()->hasRole('Administrador')) {
                session()->put('descargado', 1);
                if ($request['origen'] == 'base') {
                    $folio = Folio::find($request['id']);
                    $file = Storage::disk('archivador')->download($folio->archivo);
                    return $file;
                } elseif ($request['origen'] == 'ws') {
                    $file = Storage::disk('archivador')->get($request['id']);
                    if (!Auth::User()->hasRole('Administrador')) Storage::disk('archivador')->delete($request['id']);
                    $headers = [
                        'Content-Type'        => 'application/octet-stream',
                        'Content-Disposition' => 'attachment; filename=' . $request['id'],
                    ];
                    return response( $file, 200, $headers );
                }
            } else {
                // return redirect()->route('folios.index');
                $resultado = null;
                $mensaje='No se permite más de una descarga para la misma búsqueda';
                $mensajeInfo='';
                return view('folios.resultado', compact('resultado', 'mensaje', 'mensajeInfo'));
            }
        }
        if (!empty($request['comprobante'])) {
            $file = Storage::disk('comprobante')->download($request['comprobante']);
            return $file;
        }
        
    }
}
