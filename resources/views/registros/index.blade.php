@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['titulo' => 'Gestión de registros']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Gestión de Registros</div>
                    <div class="card-body">
                        @include('errors')
                        @can('crear-registros')
<!--
                            <div class="row">
                                <div class="col-lg-12 mb-1">
                                    <a class="btn btn-success float-right"
                                        href="{{ route('registroCivil.create') }}">Nuevo&nbsp;&nbsp;<span
                                            class="fa fa-plus"></span></a>
                                </div>
                            </div>
-->
                        @endcan
                        <table class="table table-striped table-responsive w-100 d-md-table">
                            <thead>
                                <th class="text-right" style="color:#fff;">Código</th>
                                <th class="text-left" style="color:#fff;">Registro</th>
                                <th class="text-left" style="color:#fff;">Email</th>
                                <th class="text-left" style="color:#fff;">Carpeta</th>
                                <th class="text-left" style="color:#fff;">Donde Consutar</th>
                                <th class="text-center" style="color:#fff;">Acciones</th>
                            </thead>
                            <tbody>
                                @foreach ($registros as $item)
                                    <tr>
                                        <td class="text-right">{{ $item->codigoRegistro }}</td>
                                        <td>{{ $item->nombreRegistro }}</td>
                                        <td>{{ $item->emailRegistro }}</td>
                                        <td>{{ $item->carpetaRegistro }}</td>
                                        @if(!empty($item->donde_consultar))
                                        <td>{{ $donde_consultar[$item->donde_consultar] }}</td>
                                        @else
                                        <td>&nbsp;</td>
                                        @endif
                                        <td class="text-center">
                                            @if (!empty($item->carpetaRegistro))
                                                <a title="Importar Escaneados" class="fa fa-file-import"
                                                    href="{{ route('registroCivil.importar', $item->id) }}"></a>
                                            @endif
                                            &nbsp;&nbsp;
                                            @can('editar-registros')
                                                <a title="Editar" class="fa fa-pen"
                                                    href="{{ route('registroCivil.edit', $item->id) }}"></a>
                                            @endcan
<!--
                                            &nbsp;&nbsp;
                                            @can('borrar-registros')
                                                <a title="Borrar" class="fa fa-trash"
                                                    href="{{ route('registroCivil.delete', $item->id) }}"
                                                    onclick="return confirm('¿Confirma que quiere eliminar este registro?')"></a>
                                            @endcan
-->                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
