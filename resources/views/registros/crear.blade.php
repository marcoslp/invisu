@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['link' => route('registroCivil.index'), 'titulo' => 'Gestión de registros'], ['titulo' => 'Cargar registro']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Cargar registro</div>
                    <div class="card-body">
                        @include('errors')
                        {!! Form::open(['route' => 'registroCivil.store', 'method' => 'POST']) !!}
                        @csrf
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="codigoRegistro" class="form-label">Código del registro</label>
                                {{ Form::number('codigoRegistro', null, ['class' => 'form-control', 'minlength' => '7', 'maxlength' => '7', 'required']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="nombreRegistro" class="form-label">Nombre</label>
                                {{ Form::text('nombreRegistro', null, ['class' => 'form-control', 'minlength' => '3', 'maxlength' => '30', 'required']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="emailRegistro" class="form-label">Email</label>
                                {{ Form::text('emailRegistro', null, ['class' => 'form-control', 'minlength' => '3', 'maxlength' => '50', 'required']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="carpetaRegistro" class="form-label">Carpeta Archivos</label>
                                {{ Form::select('carpetaRegistro', $carpetasRegistros, null, ['placeholder' => 'Seleccione una opción', 'class' => 'form-control']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="donde_consultar" class="form-label">Donde Consultar</label>
                                {{ Form::select('donde_consultar', $donde_consultar, null, ['placeholder' => 'Seleccione una opción', 'class' => 'form-control', 'required']) }}
                            </div>
                        </div>                                                
                        <hr>
                        <div class="row">
                            <div class="col text-right">
                                <a class="btn btn-danger" href="{{ route('registroCivil.index') }}">Cancelar&nbsp;&nbsp;<i class="fa fa-times"
                                        aria-hidden="true"></i></a>
                                <button type="submit" class="btn btn-success">Guardar&nbsp;&nbsp;<i
                                        class="fa fa-save"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
