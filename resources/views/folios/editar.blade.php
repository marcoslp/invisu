@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['link' => route('folios.index'), 'titulo' => 'Gestión de folios'], ['titulo' => 'Editar folio']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Editar Folio</div>
                    <div class="card-body">
                        @include('errors')
                        {!! Form::model($folio, ['method' => 'PATCH', 'route' => ['folios.update', $folio->id], 'files' => true]) !!}
                        @csrf
                        {!! Form::hidden('estado', 'activo') !!}
                        @if (empty(Auth::user()->registro_id))
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="registro_id" class="form-label">Registro</label>
                                    {{ Form::select('registro_id', $registros, null, ['placeholder' => 'Seleccione una opción', 'class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                        @endif
                        @if (!empty(Auth::user()->registro_id))
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="registro_id"
                                        class="form-label">{{ $registros[Auth::user()->registro_id] }}</label>
                                </div>
                            </div>
                            {!! Form::hidden('registro_id', Auth::user()->registro_id) !!}
                        @endif
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="numeroMatricula" class="form-label">Número de Matrícula</label>
                                {{ Form::text('numeroMatricula', null, ['class' => 'form-control', 'minlength' => '1', 'maxlength' => '25', 'required']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="col-lg-6">
								@if ($folio->archivo)
									<span>Esta matricula contiene un archivo cargado, puede descargarlo para ver su contenido </span>
									<a title="Descargar" class="fa fa-download" href="{{ route('postdescargar', ['id' => $folio->id, 'origen' => 'base']) }}"></a>
								@endif
								<br>
								<br>
                                <label for="archivo" class="form-label">Al seleccionar otro archivo reemplazará el actual.</label>
                                {{ Form::file('archivo', null, ['class' => 'form-control', 'accept' => '.pdf']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col text-right">
                                <a class="btn btn-danger" href="{{ route('folios.index') }}">Cancelar&nbsp;&nbsp;<i class="fa fa-times"
                                        aria-hidden="true"></i></a>
                                <button type="submit" class="btn btn-success">Guardar&nbsp;&nbsp;<i
                                        class="fa fa-save"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
