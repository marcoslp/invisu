@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['titulo' => 'Consulta IN VISU']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Consulta IN VISU</div>
                    <div class="card-body">
                        @include('errors')
                        {!! Form::open(['route' => 'postconsulta', 'method' => 'POST', 'files' => true]) !!}
                        <div class="row">
                            <div class="form-group mx-sm-3">
                                <label for="registro_id" class="form-label">Registro</label>
                                {{ Form::select('registro_id', $registros, null, ['placeholder' => 'Seleccione una opción', 'class' => 'form-control', 'required']) }}
                            </div>
                            <div class="form-group mx-sm-3">
                                <label for="matricula_id" class="form-label">Número de matrícula</label>
                                {{ Form::text('matricula_id', null, ['class' => 'form-control', 'minlength' => '1', 'maxlength' => '25', 'required']) }}
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="form-group mx-sm-3">
                                <label for="ordenPago" class="form-label">Orden de pago</label>
                                {{ Form::file('ordenPago', old('ordenPago'), ['class' => 'form-control', 'accept' => '.pdf, .png, .jpg, .jpeg', 'required']) }}
                            </div>
                            <div class="form-group mx-sm-3">
                                <label for="comprobantePago" class="form-label">Comprobante de pago</label>
                                {{ Form::file('comprobantePago', old('comprobantePago'), ['class' => 'form-control', 'accept' => '.pdf, .png, .jpg, .jpeg', 'required']) }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col text-right">
                                <a class="btn btn-default" href="{{ route('consulta') }}">Limpiar&nbsp;&nbsp;<span
                                        class="fa fa-eraser"></span></a>
                                <button type="submit" class="btn btn-secondary">Buscar&nbsp;&nbsp;<span
                                        class="fa fa-search"></span></button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
