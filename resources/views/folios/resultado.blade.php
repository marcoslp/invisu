@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['link' => route('consulta'), 'titulo' => 'Consulta IN VISU'], ['titulo' => 'Resultado']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Resultado de la consulta IN VISU</div>
                    <div class="card-body">
                        @if (!empty($resultado[0]))
                            <table class="table table-striped table-resposive w-100">
                                <thead>
                                    <th class="text-left" style="color:#fff;">Registro</th>
                                    <th class="text-center" style="color:#fff;">Fecha de digitalización</th>
                                    <th class="text-right" style="color:#fff;">Número de matrícula</th>
                                    <th class="text-center" style="color:#fff;">Acciones</th>
                                </thead>
                                <tbody>
                                    @foreach ($resultado as $value)
                                        <tr>
                                            <td>{{ $value->registro }}</td>
                                            <td class="text-center">{{ $value->fecha_alta }}</td>
                                            <td class="text-right">{{ $value->numero }}</td>
                                            <td class="text-center">
                                                <a title="Descargar" class="fa fa-download"
                                                    href="{{ route('postdescargar', ['id' => $value->id, 'origen' => $value->origen]) }}"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                        @if (!empty($mensaje))
                            <p class="text-justify"> {{ $mensaje }}</p>
                            <p class="text-justify"><i>{{ $mensajeInfo }}</i></p>
                        @endif
                        <hr>
                        <div class="row">
                            <div class="col text-right">
                                <a class="btn btn-secondary" href="{{ route('consulta') }}">Consultar otra matrícula&nbsp;&nbsp;<span
                                        class="fa fa-search"></span></a>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endsection
