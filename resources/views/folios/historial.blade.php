@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['titulo' => 'Auditoría de consultas realizadas']]) }}
<div class="container-fluid">
    <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Auditoría de consultas realizadas</div>
                    <div class="card-body">
									@include('errors')
									{!! Form::open(['route' => 'historialComprobantes', 'method' => 'GET','role'=>'search']) !!}
									@csrf
									<div class="form-group row">
										<div class="col-lg-3">
											<label for="busquedaRegistro_id" class="form-label">Registro</label>
											{{ Form::select('busquedaRegistro_id', $registros, @$input['busquedaRegistro_id'], ['class' => 'form-control', 'placeholder' => 'Seleccione una opción']) }}
										</div>
										<div class="col-lg-3">
											<label for="busquedaMatricula" class="form-label">Número de matrícula</label>
											{{ Form::text('busquedaMatricula', @$input['busquedaMatricula'], ['class' => 'form-control', 'placeholder' => 'Número de matrícula']) }}
										</div>
										<div class="col-lg-3">
											<label for="busquedaUsuario" class="form-label">Usuario</label>
											{{ Form::text('busquedaUsuario', @$input['busquedaUsuario'], ['class' => 'form-control', 'placeholder' => 'Usuario']) }}
										</div>
										 <div class="col-lg-3">
											<label for="busquedaResultado" class="form-label">Resultado</label>
											{{ Form::select('busquedaResultado', ['Encontrado' => 'Encontrado', 'No se encontro' => 'No se encontró'], @$input['busquedaResultado'], ['class' => 'form-control', 'placeholder' => 'Seleccione una opción']) }}
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6 mt-4">
											<label class="form-label">Fecha de la consulta IN VISU</label><br>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<label for="busquedaFechaI" class="form-label">Desde</label>
											{{ Form::date('busquedaFechaI', @$input['busquedaFechaI'], ['class' => 'form-control']) }}
										</div>
										<div class="col-lg-6">
											<label for="busquedaFechaF" class="form-label">Hasta</label>
											{{ Form::date('busquedaFechaF', @$input['busquedaFechaF'], ['class' => 'form-control']) }}
										</div>
									</div>
									<br><div class="row">
											<div class="col text-right">
											<a class="btn btn-default" href="{{ route('folios.limpiarHistorial') }}">Limpiar&nbsp;&nbsp;<span class="fa fa-eraser"></span></a></a>
											<button type="submit" class="btn btn-secondary">Buscar&nbsp;&nbsp;<span class="fa fa-search"></span></button></button>
											</div>
									</div>
								{!! Form::close() !!}
								<hr>
								<table class="table table-striped table-responsive w-100 d-md-table">
										<thead>
												<th class="text-center" style="color:#fff;">Fecha de la consulta IN VISU</th>
												<th class="text-center" style="color:#fff;">Número de Matrícula</th>
												<th class="text-left" style="color:#fff;">Resultado</th>
												<th class="text-left" style="color:#fff;">Usuario</th>
												<th class="text-left" style="color:#fff;">Registro</th>
												<th class="text-center" style="color:#fff;">Orden de pago</th>
												<th class="text-center" style="color:#fff;">Comprobante de pago</th>
										</thead>
										<tbody>
												@if( count($comprobantes) == 0)
													<tr><td colspan="7" class="text-center">No se encontraron datos</td></tr>
												@endif
												@foreach ($comprobantes as $value)
												<tr>
														<td class="text-center">{{ date('d/m/Y h:i:s', strtotime($value->created_at)) }}</td>
														<td class="text-right">{{ $value->matricula_id }}</td>
														<td>{{ $value->resultadoBusqueda }}</td>
														<td>{{ $value->name }}</td>
														<td>{{ $value->nombreRegistro }}</td>
														<td class="text-center">
																<a title="Descargar" class="btn btn-default" href="{{ route('postdescargar', ['comprobante' => $value->ordenPago]) }}"><span class="fa fa-download"></span></a>
														</td>
														<td class="text-center">
																<a title="Descargar" class="btn btn-default" href="{{ route('postdescargar', ['comprobante' => $value->comprobantePago]) }}"><span class="fa fa-download"></span></a>
														</td>
														</td>
												</tr>
												@endforeach
										</tbody>
								</table>
								@if(count($comprobantes) > 0)
								<div class="pagination justify-content-end">
										{!! $comprobantes->appends($input)->links() !!}
								</div>
								@endif
							</div>
            </div>
        </div>
    </div>
@endsection
