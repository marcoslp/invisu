@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['titulo' => 'Gestión de folios']]) }}
<div class="container-fluid">
    <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Gestión de folios</div>
                    <div class="card-body">
										@include('errors')								
										{!! Form::open(['route' => 'folios.index', 'method' => 'GET','role'=>'search']) !!}
										@csrf
										<div class="row">
											@if (empty(Auth::user()->registro_id))
											<div class="col-lg-6">
												<label for="busquedaRegistro_id" class="form-label">Registro</label>
												{{ Form::select('busquedaRegistro_id', $registros, @$input['busquedaRegistro_id'], ['class' => 'form-control', 'placeholder' => 'Seleccione una opción']) }}
											</div>
											@endif
											<div class="col-lg-6">
												<label for="busquedaMatricula" class="form-label">Número de matrícula</label>
												{{ Form::text('busquedaMatricula', @$input['busquedaMatricula'], ['class' => 'form-control', 'minlength' => '1', 'maxlength' => '25', 'placeholder' => 'Número de matrícula']) }}
											</div>
										</div>
										
										<div class="row">
											<div class="col-lg-6 mt-4">
												<label class="form-label">Fecha de digitalización</label><br>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<label for="busquedaFechaScaneoI" class="form-label">Desde</label>
												{{ Form::date('busquedaFechaScaneoI', @$input['busquedaFechaScaneoI'], ['class' => 'form-control']) }}
											</div>
											<div class="col-lg-6">
												<label for="busquedaFechaScaneoF" class="form-label">Hasta</label>
												{{ Form::date('busquedaFechaScaneoF', @$input['busquedaFechaScaneoF'], ['class' => 'form-control']) }}
											</div>
										</div>
										<br><div class="row">
												<div class="col text-right">
												<a class="btn btn-default" href="{{ route('folios.limpiar') }}">Limpiar&nbsp;&nbsp;<span class="fa fa-eraser"></span></a>
												<button type="submit" class="btn btn-secondary">Buscar&nbsp;&nbsp;<span class="fa fa-search"></span></button>
												</div>
										</div>
                    {!! Form::close() !!}
                    <hr>
                    @can('crear-folio')
										<div class="row mb-1">
											<div class="col-lg-12">
												<a class="btn btn-success float-right" href="{{ route('folios.create') }}">Nuevo&nbsp;&nbsp;<span class="fa fa-plus"></span></a>
											</div>
										</div>
                    @endcan
                    <table class="table table-striped table-responsive w-100 d-md-table">
                        <thead>
                            <th class="text-right" style="color:#fff;">Número de matrícula</th>
                            <th class="text-left" style="color:#fff;">Registro</th>
                            <th class="text-center" style="color:#fff;">Fecha de digitalización</th>
                            <th class="text-center" style="color:#fff;">Archivo</th>
                            <th class="text-center" style="color:#fff;">
														@can('editar-folio', 'borrar-folio')
															Acciones
														@endcan
														</th>
                        </thead>
                        <tbody>
												@if(count($folios) == 0)
												<tr><td colspan="5" class="text-center">No se encontraron datos</td></tr>
												@endif
                            @foreach ($folios as $value)
                            <tr>
                                <td class="text-right">{{ $value->numeroMatricula }}</td>
                                <td>{{ $value->nombreRegistro }}</td>
																<td class="text-center">{{ date('d/m/Y h:i:s', strtotime($value->updated_at)) }}</td>
																<td class="text-center">
                                   <a title="Descargar" class="fa fa-download" href="{{ route('postdescargar', ['id' => $value->id, 'origen' => $value->origen ]) }}"></a>
                                </td>
                                <td class="text-center">
									@can('editar-folio')
                                    <a title="Editar" class="fa fa-pen" href="{{ route('folios.edit', $value->id) }}"></a>
                                    @endcan
									&nbsp;&nbsp;
									@can('borrar-folio')
									<a title="Borrar" class="fa fa-trash" href="{{ route('folios.delete', $value->id) }}" onclick="return confirm('¿Confirma que quiere eliminar este folio?')"></a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                        @if (count($folios) > 0)
                            <div class="pagination justify-content-end">
                                {!! $folios->appends($input)->links() !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
