@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['titulo' => 'Gestión de roles']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                <div class="card-header">Gestión de Roles</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 mb-1">
                        @can('crear-rol')
                            <a class="btn btn-success float-right" href="{{ route('roles.create') }}">Nuevo&nbsp;&nbsp;<span class="fa fa-plus"></span></a>
                        @endcan
                            </div>
                        </div>
                    <table class="table table-striped table-resposive">
                        <thead>
                                <th class="text-left" style="color:#fff;">Nombre</th>
                                <th class="text-center" style="color:#fff;">Acciones</th>
                            </thead>
                            <tbody>
                                @foreach ($roles as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td class="text-center">                                            
                                            @can('editar-rol')
                                            <a title="Editar" class="fa fa-pen" href="{{ route('roles.edit', $item->id) }}"></a>
                                            @endcan
                                            &nbsp;&nbsp;
                                            @can('borrar-rol')
                                            <a title="Borrar" class="fa fa-trash" href="{{ route('roles.delete', $item->id) }}" onclick="return confirm('¿Confirma que quiere eliminar este rol?')"></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination justify-content-end">
                            {!! $roles->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
