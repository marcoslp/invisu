@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['link' => route('roles.index'), 'titulo' => 'Gestión de roles'], ['titulo' => 'Cargar rol']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Cargar rol</div>
                    <div class="card-header"></div>
                    <div class="card-body">
                        @include('errors')
                        {!! Form::open(['route' => 'roles.store', 'method' => 'POST']) !!}
                        @csrf
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="name" class="form-label">Nombre</label>
                                {{ Form::text('name', null, ['class' => 'form-control', 'minlength' => '3', 'maxlength' => '15', 'required']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="" class="form-label">Permisos</label>
                                <br>
                                @foreach ($permission as $value)
                                    <label>{{ Form::checkbox('permission[]', $value->id, false, ['class' => 'name']) }}
                                        {{ $value->name }}</label>
                                    <br />
                                @endforeach
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col text-right">
                                <a class="btn btn-danger" href="{{ route('roles.index') }}">Cancelar&nbsp;&nbsp;<i class="fa fa-times"
                                        aria-hidden="true"></i></a>
                                <button type="submit" class="btn btn-success">Guardar&nbsp;&nbsp;<i
                                        class="fa fa-save"></i></button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
