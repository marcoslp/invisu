@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['link' => route('usuarios.index'), 'titulo' => 'Gestión de usuarios'], ['titulo' => 'Editar usuario']]) }}
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">
                <div class="card">
                <div class="card-header">Editar Usuario</div>
                    <div class="card-body">
                        @include('errors')
                        {!! Form::model($user, ['method' => 'PUT', 'route' => ['usuarios.update', $user->id]]) !!}
                        @csrf
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="roles" class="form-label">Roles</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                @if ($user->id > 1)
                                {!! Form::select('roles[]', $roles, $userRole, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción']) !!}
                                @endif
                                @if ($user->id == 1)
                                {!! Form::select('roles[]', $roles, $userRole, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción' , 'disabled' => true]) !!}
                                @endif
                            </div>
                        </div>
                        <br><div class="row">
                            <div class="col-lg-5">
                                <label for="roles" class="form-label">Registro</label>
                                {!! Form::select('registro_id', $registros, $user->registro_id, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción']) !!}
                            </div>
                        </div>
                        <br><div class="row">
                            <div class="col-lg-5">
                                <label for="documento" class="form-label">Número de Documento</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::number('documento', null, ['class' => 'form-control', 'minlength' => '5', 'maxlength' => '12', 'required']) }}
                                <i>Debe tener un mínimo de 5 números y un máximo de 12</i>
                            </div>
                        </div>
                        <br><div class="row">
                            <div class="col-lg-5">
                                <label for="name" class="form-label">Nombre y Apellido</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::text('name', null, ['class' => 'form-control', 'minlength' => '3', 'maxlength' => '50', 'required']) }}
                                <i>Debe tener un mínimo de 3 caracteres y un máximo de 50</i>
                            </div>
                        </div>
                        <br><div class="row">
                            <div class="col-lg-5">
                                <label for="email" class="form-label">Email</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::text('email', null, ['class' => 'form-control', 'minlength' => '3', 'maxlength' => '50', 'required']) }}
                            </div>
                        </div>
                        <hr>
                    <div class="row">
						<div class="col text-right">
							<a class="btn btn-danger" href="{{ route('usuarios.index') }}">Cancelar&nbsp;&nbsp;<i class="fa fa-times" aria-hidden="true"></i></a>
							<button type="submit" class="btn btn-success">Guardar&nbsp;&nbsp;<i class="fa fa-save"></i></button>
						</div>
					</div>
                        {!! Form::close() !!}
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
<script>
function mostrar_ocultar_pass(e, t) {
  if (e.type == 'text') {
    var tipo = 'password';
    var tag = 'Mostrar'
  } else {
    var tipo = 'text';
    var tag = 'Ocultar'
  }
  e.type = tipo;
  t.textContent = tag;
}
</script>