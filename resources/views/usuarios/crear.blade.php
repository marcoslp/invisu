@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['link' => route('usuarios.index'), 'titulo' => 'Gestión de usuarios'], ['titulo' => 'Cargar usuario']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Cargar usuarios</div>
                    <div class="card-body">
                        @include('errors')
                        {!! Form::open(['route' => 'usuarios.store', 'method' => 'POST']) !!}
                        @csrf
                        {!! Form::hidden('estado', 'activo') !!}
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="roles" class="form-label">Roles</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {!! Form::select('roles', $roles, null, ['class' => 'form-control', 'placeholder' => 'Seleccione una opción']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="roles" class="form-label">Registro</label>
                                {!! Form::select('registro_id', $registros, null, [
                                    'class' => 'form-control',
                                    'placeholder' => 'Seleccione una opción',
                                ]) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="documento" class="form-label">Número de
                                    Documento</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::number('documento', null, ['class' => 'form-control']) }}
                                <i>Debe tener un mínimo de 5 números y un máximo de 12</i>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="name" class="form-label">Nombre y
                                    Apellido</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                                <i>Debe tener un mínimo de 3 caracteres y un máximo de 50</i>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="email" class="form-label">Email</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::text('email', null, ['class' => 'form-control', 'minlength' => '3', 'maxlength' => '50', 'required']) }}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="password" class="form-label">Contraseña</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::password('password', ['class' => 'form-control', 'id' => 'clave']) }}
                                <span
                                    style="position: absolute;right: 15px;top: 37px;padding: 1px 5px;background-color: #dadada;cursor: pointer;"
                                    onclick="mostrar_ocultar_pass(document.getElementById('clave'), this);">Mostrar</span>
                                <i>La contraseña debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una
                                    minúscula y al menos una mayúscula. NO puede tener otros símbolos.</i>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-5">
                                <label for="confirm-password" class="form-label">Confirmar
                                    contraseña</label>&nbsp;&nbsp;<i>(Obligatorio)</i>
                                {{ Form::password('confirm-password', ['class' => 'form-control', 'id' => 'clave_confirmar']) }}
                                <span
                                    style="position: absolute;right: 15px;top: 37px;padding: 1px 5px;background-color: #dadada;cursor: pointer;"
                                    onclick="mostrar_ocultar_pass(document.getElementById('clave_confirmar'), this);">Mostrar</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col text-right">
                                <a class="btn btn-danger" href="{{ route('usuarios.index') }}">Cancelar&nbsp;&nbsp;<i class="fa fa-times"
                                        aria-hidden="true"></i></a>
                                <button type="submit" class="btn btn-success">Guardar&nbsp;&nbsp;<i
                                        class="fa fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
<script>
    function mostrar_ocultar_pass(e, t) {
        if (e.type == 'text') {
            var tipo = 'password';
            var tag = 'Mostrar'
        } else {
            var tipo = 'text';
            var tag = 'Ocultar'
        }
        e.type = tipo;
        t.textContent = tag;
    }
</script>
