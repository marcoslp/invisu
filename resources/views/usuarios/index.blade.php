@extends('adminlte::page')

@section('content')
{{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['titulo' => 'Gestión de usuarios']]) }}
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
			<div class="card-header">Gestión de usuarios</div>
                <div class="card-body">
					@include('errors')
                    {!! Form::open(['route' => 'usuarios.index', 'method' => 'GET','role'=>'search']) !!}
					@csrf
										<div class="row">
											<div class="col-lg-4">
												<label for="busquedaNombre" class="form-label">Número de documento</label>
												{{ Form::number('busquedaDNI', @$input['busquedaDNI'], ['class' => 'form-control', 'placeholder' => 'Número de documento', 'pattern' =>'[0-9]+', 'minlength' => '6', 'maxlength' => '8']) }}
											</div>
											<div class="col-lg-4">
												<label for="busquedaNombre" class="form-label">Nombre y apellido</label>
												{{ Form::text('busquedaNombre', @$input['busquedaNombre'], ['class' => 'form-control', 'placeholder' => 'Nombre', 'minlength' => '3', 'maxlength' => '50']) }}
											</div>
											<div class="col-lg-4">
												<label for="busquedaEmail" class="form-label">Email</label>
												{{ Form::text('busquedaEmail', @$input['busquedaEmail'], ['class' => 'form-control', 'placeholder' => 'Email', 'minlength' => '3', 'maxlength' => '50']) }}
											</div>
											<div class="col-lg-4">
												<label for="busquedaRol" class="form-label">Rol</label>
												{{ Form::select('busquedaRol', $roles, @$input['busquedaRol'], ['class' => 'form-control', 'placeholder' => 'Seleccione una opción' ]) }}
											</div>
											<div class="col-lg-4">
												<label for="busquedaEstado" class="form-label">Estado</label>
												{{ Form::select('busquedaEstado', ['activo' => 'Activo', 'inactivo' => 'Inactivo'], @$input['busquedaEstado'], ['class' => 'form-control', 'placeholder' => 'Seleccione una opción' ]) }}
											</div>
											<div class="col-lg-4">
												<label for="busquedaRegistro" class="form-label">Registro</label>
												{{ Form::select('busquedaRegistro', $registros, @$input['busquedaRegistro'], ['class' => 'form-control', 'placeholder' => 'Seleccione una opción' ]) }}
											</div>
										</div>
										<br>
                        <div class="row">
                            <div class="col text-right">
                            <a class="btn btn-default" href="{{ route('usuarios.index') }}">Limpiar&nbsp;&nbsp;<span class="fa fa-eraser"></span></a></a>
                            <button type="submit" class="btn btn-secondary">Buscar&nbsp;&nbsp;<span class="fa fa-search"></span></button></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <hr>
					@can('crear-usuarios')
					<div class="row mb-1">
						<div class="col-lg-12">
														<a class="btn btn-success float-right" href="{{ route('usuarios.create') }}">Nuevo&nbsp;&nbsp;<span class="fa fa-plus"></span></a>
						</div>
					</div>
					@endcan
                    <table class="table table-striped table-responsive w-100 d-md-table">
						<thead>
							<th class="text-right" style="color:#fff;">Número de documento</th>
							<th class="text-left" style="color:#fff;">Nombre y apellido</th>
							<th class="text-left" style="color:#fff;">Email</th>
							<th class="text-left" style="color:#fff;">Rol</th>
							<th class="text-left" style="color:#fff;">Registro</th>
							<th class="text-left" style="color:#fff;">Estado</th>
							<th class="text-center" style="color:#fff;">Acciones</th>
						</thead>
						<tbody>
								@if(count($usuarios) < 0)
									<tr><td colspan="6" class="text-center">No se encontraron datos</td></tr>
								@endif
								@foreach ($usuarios as $item)
										<tr>
												<td class="text-right">{{ $item->documento }}</td>
												<td>{{ $item->name }}</td>
												<td>{{ $item->email }}</td>
												<td>
													@if (!empty($item->getRoleNames()))
														@foreach ($item->getRoleNames() as $rolName)
															<span>{{ $rolName }}</span>
														@endforeach
													@endif
												</td>
												<td>@if (!empty($item->registro_id)) {{ $registros[$item->registro_id] }} @endif</td>
												<td>{{ ucwords($item->estado) }}</td>
												<td class="text-center">
														@can('editar-usuarios')
															<a title="Editar" class="fa fa-pen" href="{{ route('usuarios.edit', $item->id) }}"></a>
														@endcan
														&nbsp;&nbsp;
														@can('borrar-rol')
															@if ($item->estado == 'activo' && $item->id != Auth::user()->id)
																<a title="Desactivar" class="fa fa-toggle-off" href="{{ route('usuarios.delete', $item->id) }}" onclick="return confirm('¿Confirma que quiere desactivar este usuario?')"></a>
															@endif
															@if ($item->estado == 'inactivo' && $item->id != Auth::user()->id)
																<a title="Activar" class="fa fa-toggle-on" href="{{ route('usuarios.delete', $item->id) }}" onclick="return confirm('¿Confirma que quiere activar este usuario?')"></a>
															@endif
														@endcan
												</td>
										</tr>
								@endforeach
						</tbody>
					</table>
					@if(count($usuarios) > 0)
					<div class="pagination justify-content-end">
						{!! $usuarios->appends($input)->links() !!}
					</div>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
