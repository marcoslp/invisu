@extends('adminlte::page')

@section('content')
    {{ mostrar_miga([['link' => route('home'), 'titulo' => 'Inicio'], ['titulo' => 'Contacto']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Contacto</div>
                    <div class="card-body">
                        <p class="text-justify">Ante cualquier duda contáctese con el registro correspondiente</p>
                        <p class="text-justify">Para mas información sobre el sistema de consultas IN VISU enviar un email a: <a href="mailto:visuonlinefolios-notariado@entrerios.gov.ar">visuonlinefolios-notariado@entrerios.gov.ar</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection