@extends('adminlte::page')

@section('content')
    {{ mostrar_miga([['titulo' => 'Inicio']]) }}
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Le damos la bienvenida</div>
                    <div class="card-body">
                        <p class="text-justify">Para acceder al VISU del folio de una propiedad debe ingresar por la opción Consulta IN VISU y completar el número de matrícula. Recuerde no usar separadores de miles. En el caso de que se trate de una propiedad con polígonos se ingresan uno junto al otro sin guiones y con un espacio luego del número de matrícula Por ejemplo: 135230 00010101</p>
                        <p class="text-justify">Ante cualquier duda contáctenos a la siguiente dirección de mail <a href="mailto:visuonlinefolios-notariado@entrerios.gov.ar">visuonlinefolios-notariado@entrerios.gov.ar</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
