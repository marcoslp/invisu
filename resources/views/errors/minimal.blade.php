<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">

    <style type="text/css">
        body{
          margin-top: 150px;
					background-color: #799f4f;
        }
        .error-main{
          background-color: #fff;
          box-shadow: 0px 10px 10px -10px #5D6572;
        }
        .error-main h1{
          font-weight: bold;
          color: #444444;
          font-size: 100px;
          text-shadow: 2px 4px 5px #6E6E6E;
        }
        .error-main h6{
          color: #42494F;
        }
        .error-main p{
          color: #9897A0;
          font-size: 14px; 
        }
    </style>
    </head>
    <body>
    <div class="container">
			<div style="width: 100%; height: 100%; position: absolute; top:0px; left:0px; background-image: url({{ config('app.url') }}/img/trama.png);opacity: 0.05;"></div>
      <div class="row text-center">
        <div class="col-lg-6 offset-lg-3 col-sm-6 offset-sm-3 col-12 p-3 error-main">
          <div class="row">
            <div class="col-lg-8 col-12 col-sm-10 offset-lg-2 offset-sm-1">
                <h1 class="m-0">
                    @yield('code')
                </h1>
                <h6>@yield('message')</h6>
                <hr>
                <p><a class="btn btn-light" href="{{ route('home') }}">Ir al inicio</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    </body>
</html>
