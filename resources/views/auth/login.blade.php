@extends('auth.auth-page', ['auth_type' => 'login'])
@section('adminlte_css_pre')
    <link rel="stylesheet" href="{{ asset('vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@stop

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )
@php( $password_reset_url = View::getSection('password_reset_url') ?? config('adminlte.password_reset_url', 'password/reset') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
@endif

@section('auth_header', __('adminlte::adminlte.login_message'))

@section('auth_body')
    <form action="{{ $login_url }}" method="post">
        @csrf
        
        @if($errors->has('active'))
            <div class="alert alert-danger">
                {{ $errors->first('active') }}
            </div>
        @endif

        @if ($errors->has('captcha'))
            <div class="alert alert-danger">
                {{ $errors->first('captcha') }}
            </div>
        @endif
        

        {{-- Email field --}}
        <div class="input-group mb-3">
            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                   value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}" autofocus>

            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        {{-- Password field --}}
        <div class="input-group mb-3">
            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror"
                   placeholder="{{ __('adminlte::adminlte.password') }}">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span
                        style="z-index:100; position: absolute;right: 5px;top: 5px;padding: 1px 5px;background-color: #dadada;cursor: pointer;"
                        onclick="mostrar_ocultar_pass(document.getElementById('password'), this);">Mostrar
                    </span>
                </div>
            </div>

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group row">
            <div class="col-md-5 ml-2">
                {!! NoCaptcha::renderJs() !!}
                {!! NoCaptcha::display() !!}
            </div>
        </div>
        
        {{-- Login field --}}
        <div class="row" style="text-align: -webkit-center;display: block;">
            <div class="col-12">
                <button type=submit class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
                    <span class="fas fa-sign-in-alt"></span>
                    {{ __('adminlte::adminlte.sign_in') }}
                </button>
            </div>
        </div>
    </form>
        <p class="text-center h5">
            <a href="mailto:visuonlinefolios-notariado@entrerios.gov.ar">Solicitar Usuario</a>
        </p>
@stop
<script>
    function mostrar_ocultar_pass(e, t) {
        if (e.type == 'text') {
            var tipo = 'password';
            var tag = 'Mostrar'
        } else {
            var tipo = 'text';
            var tag = 'Ocultar'
        }
        e.type = tipo;
        t.textContent = tag;
    }
</script>


@section('auth_footer')
    {{-- Password reset link --}}
    @if($password_reset_url)
        <p class="my-0 text-center">
            <a href="{{ $password_reset_url }}">
                {{ __('adminlte::adminlte.i_forgot_my_password') }}
            </a>
        </p>
    @endif
@stop
