<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SedeerTablePermisos::class);
        $this->call(agrega_mas_permisos::class);
        // \App\Models\User::factory(10)->create();
    }
}
