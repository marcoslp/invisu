<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class agrega_mas_permisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos = [
            'ver-usuarios',
            'crear-usuarios',
            'editar-usuarios',
            'borrar-usuarios',
            'ver-auditoria',
            'consulta-invisu',
        ];  
        $role1 = Role::find(1);
        foreach ($permisos as $permiso) {
            Permission::create(['name' => $permiso]);
            $role1->givePermissionTo($permiso);
        }
    }
}
