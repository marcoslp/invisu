<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
//Spatie
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use \App\Models\registroCivil;
use \App\Models\User;

class SedeerTablePermisos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Configuracion para permisos.
        $permisos = [
            //Tabla roles
            'ver-rol',
            'crear-rol',
            'editar-rol',
            'borrar-rol',
            //Tabla folios
            'ver-folio',
            'crear-folio',
            'editar-folio',
            'borrar-folio',
            //Registros
            'ver-registros',
            'crear-registros',
            'editar-registros',
            'borrar-registros',
        ];
        foreach ($permisos as $permiso) {
            Permission::create(['name' => $permiso]);
        }
        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'Administrador']);
        $role1->givePermissionTo('ver-rol');
        $role1->givePermissionTo('crear-rol');
        $role1->givePermissionTo('editar-rol');
        $role1->givePermissionTo('borrar-rol');
        $role1->givePermissionTo('ver-folio');
        $role1->givePermissionTo('crear-folio');
        $role1->givePermissionTo('editar-folio');
        $role1->givePermissionTo('borrar-folio');
        $role1->givePermissionTo('ver-registros');
        $role1->givePermissionTo('crear-registros');
        $role1->givePermissionTo('editar-registros');
        $role1->givePermissionTo('borrar-registros');

        $user = User::factory()->create([
            'name' => 'Super Administrador',
            'email' => 'superAdmin@invisu.com.ar',
            'documento' => 12345678,
            'password' => bcrypt('admin123'),
        ]);
        $user->assignRole($role1);
        //registroCivil::create(['codigoRegistro' => '0', 'nombreRegistro' => 'Seleccione Registro', 'emailRegistro' => 'sinRegistro@invisu.com.ar']);
        registroCivil::create(['codigoRegistro' => '1006281', 'nombreRegistro' => 'REGISTRO COLON', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006282', 'nombreRegistro' => 'REGISTRO CONCORDIA', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006283', 'nombreRegistro' => 'REGISTRO DIAMANTE', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006284', 'nombreRegistro' => 'REGISTRO FEDERACION(CHAJARI)', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006285', 'nombreRegistro' => 'REGISTRO FEDERAL', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006286', 'nombreRegistro' => 'REGISTRO FELICIANO', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006287', 'nombreRegistro' => 'REGISTRO GUALEGUAY', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006288', 'nombreRegistro' => 'REGISTRO GUALEGUAYCHU', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006289', 'nombreRegistro' => 'REGISTRO LA PAZ', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006290', 'nombreRegistro' => 'REGISTRO NOGOYA', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006280', 'nombreRegistro' => 'REGISTRO PARANA', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006292', 'nombreRegistro' => 'REGISTRO SAN SALVADOR', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006291', 'nombreRegistro' => 'REGISTRO TALA', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006293', 'nombreRegistro' => 'REGISTRO URUGUAY', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006294', 'nombreRegistro' => 'REGISTRO VICTORIA', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
        registroCivil::create(['codigoRegistro' => '1006295', 'nombreRegistro' => 'REGISTRO VILLAGUAY', 'emailRegistro' => 'visuonlinefolios-notariado@entrerios.gov.ar']);
    }
}
