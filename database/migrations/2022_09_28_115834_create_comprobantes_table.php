<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComprobantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comprobantes', function (Blueprint $table) {
            $table->id();
            $table->string('ordenPago')->nulleable(true);
            $table->string('comprobantePago')->nulleable(true);
            $table->string('usuarioConsultor')->nulleable(true);
            $table->unsignedBigInteger('registro_id')->nullable(true);
            $table->string('matricula_id')->nulleable(true);
            $table->string('resultadoBusqueda')->nulleable(false);
            $table->timestamps();

            //Relacion FK con registros civiles
            $table->foreign('registro_id')
                ->references('id')
                ->on('registro_civils')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comprobantes');
    }
}
