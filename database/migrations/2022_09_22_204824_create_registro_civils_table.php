<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistroCivilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_civils', function (Blueprint $table) {
            $table->id();
            $table->string('codigoRegistro')->nulleable(false);
            $table->string('nombreRegistro')->nulleable(false);
            $table->string('emailRegistro')->nulleable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_civils');
    }
}
