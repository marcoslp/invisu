<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folios', function (Blueprint $table) {
            $table->id();
            $table->string('numeroMatricula')->nullable(true);
            $table->unsignedBigInteger('registro_id')->nullable(true); //debe coincidir con alguno de los registros
            $table->string('fechaScaneo')->nullable(true); #Sirve para identificar los recien escaneados
            $table->string('estado')->nullable(true);
            //Relacion FK con registros
            $table->foreign('registro_id')
                ->references('id')
                ->on('registro_civils')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folios');
    }
}
